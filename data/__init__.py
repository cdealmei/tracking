from .wider_face import WiderFaceDetection, detection_collate
from .data_augment import *
from .config import *
from .scut_head import ScutHeadDetection, detection_collate_scut