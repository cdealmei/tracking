#scut head

import os
import os.path
import sys
import torch
import torch.utils.data as data
import cv2
import numpy as np
from scipy.misc import imread
import os.path as osp
from collections import defaultdict

class ScutHeadDetection(data.Dataset):
    def __init__(self, txt_path, base_path, preproc=None):
        self.preproc = preproc
        self.base_path = base_path
        self.words = []
        self.bboxes = defaultdict(list)

        with open(txt_path, 'r') as txt:
        	lines = txt.readlines()
        	self.imgs_path = [i.rstrip().strip("#").lstrip() for i in lines if i.startswith('#')]
        	ind = -1
        	for lin in lines:
        		if lin.startswith('#'):
        			ind+=1
        			continue
        		lin_list = [int(i) for i in lin.rstrip().split(' ')]
        		self.bboxes[ind].append(lin_list)

    def __len__(self):
        return len(self.imgs_path)

    def __getitem__(self, index):
        img = imread(osp.join(self.base_path, self.imgs_path[index]))
        height, width, _ = img.shape

        labels = self.bboxes[index]
        annotations = np.zeros((0, 5))
        if len(labels) == 0:
            return annotations
        for idx, label in enumerate(labels):
            annotation = np.zeros((1, 5))
            # bbox
            annotation[0, 0] = label[0]  # x1
            annotation[0, 1] = label[1]  # y1
            annotation[0, 2] = label[2]  # x2
            annotation[0, 3] = label[3]  # y2
            annotation[0, 4] = 1
            annotations = np.append(annotations, annotation, axis=0)
        target = np.array(annotations)
        if self.preproc is not None:
            img, target = self.preproc(img, target)

        return torch.from_numpy(img), target

def detection_collate_scut(batch):
    """Custom collate fn for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    Arguments:
        batch: (tuple) A tuple of tensor images and lists of annotations

    Return:
        A tuple containing:
            1) (tensor) batch of images stacked on their 0 dim
            2) (list of tensors) annotations for a given image are stacked on 0 dim
    """
    targets = []
    imgs = []
    for _, sample in enumerate(batch):
        for _, tup in enumerate(sample):
            if torch.is_tensor(tup):
                imgs.append(tup)
            elif isinstance(tup, type(np.empty(0))):
                annos = torch.from_numpy(tup).float()
                targets.append(annos)
    import pdb;pdb.set_trace()
    return (torch.stack(imgs, 0), targets)
