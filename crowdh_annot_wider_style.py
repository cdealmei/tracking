import cv2
import numpy as np
import os
from glob import glob
import shutil
import pandas as pd
import argparse
from ast import literal_eval

from pandas.io.json import json_normalize

parser = argparse.ArgumentParser()
parser.add_argument('--path', '-p', help='Path to the directory containing '+
	'images subdirectory and the corresponding .odgt annotation file')

args = parser.parse_args()

path = args.path
annot_path = glob(path + '*.odgt')[0]

df = pd.DataFrame()
with open(annot_path) as json_file:
    ls = json_file.readlines()

with open(path + '/label.txt', 'w+') as file:
    for l in ls :
        temp_dict = literal_eval(l.replace('\n',''))
        file.write('# ' + temp_dict['ID'] + '.jpg\n')
        
        for i in range(len(temp_dict['gtboxes'])):
            
            if temp_dict['gtboxes'][i]['tag'] == 'person' :
                
                if 'ignore' in temp_dict['gtboxes'][i]['head_attr'].keys():
                    if temp_dict['gtboxes'][i]['head_attr']['ignore'] == 1 :
                        continue
                        
                if 'unsure' in temp_dict['gtboxes'][i]['head_attr'].keys():
                    
                    if 'occ' in temp_dict['gtboxes'][i]['head_attr'].keys():
                        if temp_dict['gtboxes'][i]['head_attr']['unsure'] == 0 & temp_dict['gtboxes'][i]['head_attr']['occ'] == 0 :
                            x1,x2,y1,y2 = temp_dict['gtboxes'][i]['hbox'][0], temp_dict['gtboxes'][i]['hbox'][1], temp_dict['gtboxes'][i]['hbox'][2], temp_dict['gtboxes'][i]['hbox'][3]
                            file.write(f'{x1} {x2} {y1} {y2} -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 0.2\n')
                    
                    elif temp_dict['gtboxes'][i]['head_attr']['unsure'] == 0 :
                            x1,x2,y1,y2 = temp_dict['gtboxes'][i]['hbox'][0], temp_dict['gtboxes'][i]['hbox'][1], temp_dict['gtboxes'][i]['hbox'][2], temp_dict['gtboxes'][i]['hbox'][3]
                            file.write(f'{x1} {x2} {y1} {y2} -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 0.2\n')
                        
                elif 'occ' in temp_dict['gtboxes'][i]['head_attr'].keys():
                        if temp_dict['gtboxes'][i]['head_attr']['occ'] == 0 :
                            x1,x2,y1,y2 = temp_dict['gtboxes'][i]['hbox'][0], temp_dict['gtboxes'][i]['hbox'][1], temp_dict['gtboxes'][i]['hbox'][2], temp_dict['gtboxes'][i]['hbox'][3]
                            file.write(f'{x1} {x2} {y1} {y2} -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 0.2\n')
                else :
                    x1,x2,y1,y2 = temp_dict['gtboxes'][i]['hbox'][0], temp_dict['gtboxes'][i]['hbox'][1], temp_dict['gtboxes'][i]['hbox'][2], temp_dict['gtboxes'][i]['hbox'][3]
                    file.write(f'{x1} {x2} {y1} {y2} -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 0.2\n')
